export default interface IUserData {
   id: number,
   username: string,
   email: string,
   password: string,
   class_id: number,
   created_at: string,
   updated_at: string
}