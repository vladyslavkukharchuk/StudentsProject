export default interface IUserClass {
   username: string,
   class_id: number,
   name: string,
   health: number,
   damage: number,
   attack_type: string,
   ability: string
}