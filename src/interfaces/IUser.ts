// {
//    "_id": number;
//    "username": string;
//    "hp": number;
//    "statuses": number[];
// }

export default interface IUser {
   _id: number,
   username: string,
   hp: number,
   statuses: number[]
}