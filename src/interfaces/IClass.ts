export default interface IClass {
   id: number,
   name: string,
   health: number,
   damage: number,
   attack_type: string,
   ability: string,
   created_at: string,
   updated_at: string
}